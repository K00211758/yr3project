//  Created by Conor Garrahy and Niall Murphy 1/2/18

#include "LoadingState.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "InputHandler.h"
#include "StateParser.h"

const std::string LoadingState::s_loadingID = "LOADING";

void LoadingState::s_loadingPlay()
{
	TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}

void LoadingState::s_loadingMenu()
{
	TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void LoadingState::update()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->update();
		}
	}
}

void LoadingState::render()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->draw();
		}
	}
}

bool LoadingState::onEnter()
{
	StateParser stateParser;
	stateParser.parseState("assets/attack.xml", s_loadingID, &m_gameObjects, &m_textureIDList);

	m_callbacks.push_back(0);
	m_callbacks.push_back(s_loadingPlay);
	m_callbacks.push_back(s_loadingMenu);

	setCallbacks(m_callbacks);

	m_loadingComplete = true;

	std::cout << "entering LoadingState\n";
	return true;
}

bool LoadingState::onExit()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->clean();
			delete m_gameObjects[i];
		}
		m_gameObjects.clear();
	}
	// clear the texture manager
	for (int i = 0; i < m_textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
	}
	TheInputHandler::Instance()->reset();

	std::cout << "exiting LoadingState\n";
	return true;
}

void LoadingState::setCallbacks(const std::vector<Callback>& callbacks)
{
	// go through the game objects
	if (!m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			// if they are of type MenuButton then assign a callback based on the id passed in from the file
			if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
			{
				MenuButton* pButton = dynamic_cast<MenuButton*>(m_gameObjects[i]);
				pButton->setCallback(callbacks[pButton->getCallbackID()]);
			}
		}
	}
}