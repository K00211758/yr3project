//
//  PlayState.cpp
//  SDL Game Programming Book
//  Edited by Conor Garrahy and Niall Murphy 1/2/18
//  Created by shaun mitchell on 09/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include <iostream>
#include "PlayState.h"
#include "GameOverState.h"
#include "PauseState.h"
#include "ControlsState.h"
#include "Game.h"
#include "InputHandler.h"
#include "LevelParser.h"
#include "Level.h"
#include "BulletHandler.h"

const std::string PlayState::s_playID = "PLAY";

void PlayState::update()
{
    if(m_loadingComplete && !m_exiting)
    {
        if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE))
        {
            TheGame::Instance()->getStateMachine()->pushState(new PauseState());
        }

		if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_H))
		{
			TheGame::Instance()->getStateMachine()->pushState(new ControlsState());
		}
        
//        if(TheInputHandler::Instance()->getButtonState(0, 8))
//        {
//            TheGame::Instance()->getStateMachine()->pushState(new PauseState());
//        }
        
        TheBulletHandler::Instance()->updateBullets();

		//if (TheGame::Instance()->getPlayerPower() == 0)
		//{
		//	TheGame::Instance()->setPlayerLives(TheGame::Instance()->getPlayerLives() - 1);
		//	TheGame::Instance()->setPlayerPower(3);
		//}
        
        if(TheGame::Instance()->getPlayerLives() == 0)
        {
            TheGame::Instance()->getStateMachine()->changeState(new GameOverState());
        }
        
        if(pLevel != 0)
        {
            pLevel->update();
        }
    }
}

void PlayState::render()
{
    if(m_loadingComplete)
    {
        if(pLevel != 0)
        {
            pLevel->render();
        }
        
        for(int i = 0; i < TheGame::Instance()->getPlayerLives(); i++)
        {
            TheTextureManager::Instance()->drawFrame("lives", i * 30, 0, 32, 30, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);
        }

		for (int i = 0; i < TheGame::Instance()->getPlayerPower(); i++)
		{
			TheTextureManager::Instance()->drawFrame("power", i * 30, 30, 32, 20, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);
		}

		for (int i = 0; i < TheGame::Instance()->getPlayerCoinTotal(); i++)
		{
			TheTextureManager::Instance()->drawFrame("coinTotal", i * 30, 60, 36, 35, 0, 0, TheGame::Instance()->getRenderer(), 0.0, 255);
		}
        
        TheBulletHandler::Instance()->drawBullets();
    }
}

bool PlayState::onEnter()
{

    TheGame::Instance()->setPlayerLives(3);
	TheGame::Instance()->setPlayerPower(3);
	TheGame::Instance()->setPlayerCoinTotal(0);
    
    LevelParser levelParser;
    pLevel = levelParser.parseLevel(TheGame::Instance()->getLevelFiles()[TheGame::Instance()->getCurrentLevel() - 1].c_str());
    
    TheTextureManager::Instance()->load("assets/bullet1.png", "bullet1", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/bullet2.png", "bullet2", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/bullet3.png", "bullet3", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/bullet4.png", "bullet4", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/bullet6.png", "bullet6", TheGame::Instance()->getRenderer());
    TheTextureManager::Instance()->load("assets/lives.png", "lives", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/powerbar.png", "power", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/coin1.png", "coinTotal", TheGame::Instance()->getRenderer());
	TheTextureManager::Instance()->load("assets/heart.png", "heart", TheGame::Instance()->getRenderer());
    
    if(pLevel != 0)
    {
        m_loadingComplete = true;
    }
    
    std::cout << "entering PlayState\n";
    return true;
}

bool PlayState::onExit()
{
    m_exiting = true;
    
    TheInputHandler::Instance()->reset();
    TheBulletHandler::Instance()->clearBullets();
    
    std::cout << "exiting PlayState\n";
    return true;
}
