//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef SDL_Game_Programming_Book_LoadingState_h
#define SDL_Game_Programming_Book_LoadingState_h

#include <iostream>
#include "MenuState.h"
#include <vector>

class GameObject;

class LoadingState : public MenuState
{
public:

	virtual ~LoadingState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_loadingID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_loadingPlay();
	static void s_loadingMenu();


	static const std::string s_loadingID;

	std::vector<GameObject*> m_gameObjects;
};

#endif
