//
//
// Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef SDL_Game_Programming_Book_Fireball_h
#define SDL_Game_Programming_Book_Fireball_h

#include "Enemy.h"

class Fireball : public Enemy
{
public:

	Fireball() : Enemy()
	{
		m_dyingTime = 1000;
		m_health = 5;
		m_moveSpeed = 3;
		//	m_bulletFiringSpeed = 50;
		m_gap = 90;

		// m_velocity.setY(-m_moveSpeed);
	}

	virtual ~Fireball() {}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams)
	{
		ShooterObject::load(std::move(pParams));

		//	m_velocity.setX(-m_moveSpeed);
		m_velocity.setY(m_moveSpeed / 2);

		m_maxHeight = m_position.getY() + m_gap;
		m_minHeight = m_position.getY() - m_gap;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}
		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_position.getY() + m_height >= m_maxHeight)
			{
				m_velocity.setY(-m_moveSpeed);
			}
			else if (m_position.getY() <= m_minHeight)
			{
				m_velocity.setY(m_moveSpeed);
			}

			/*	if (m_bulletCounter == m_bulletFiringSpeed)
			{
			TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-3, 0));
			m_bulletCounter = 0;
			}
			m_bulletCounter++; */

		}
		else
		{
			m_velocity.setY(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}

private:

	int m_maxHeight;
	int m_minHeight;
	int m_gap;

};

class FireballCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Fireball();
	}
};


#endif
