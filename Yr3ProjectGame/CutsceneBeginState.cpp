//
//  PauseState.cpp
//  SDL Game Programming Book
//
// Edited by Conor Garrahy and Niall Murphy 1/2/18
//

#include "CutsceneBeginState.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "InputHandler.h"
#include "StateParser.h"

const std::string CutsceneBeginState::s_beginID = "BEGIN";

void CutsceneBeginState::s_beginToMain()
{
	TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void CutsceneBeginState::s_beginToPlay()
{
	TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}

void CutsceneBeginState::update()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->update();
		}
	}
}

void CutsceneBeginState::render()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->draw();
		}
	}
}

bool CutsceneBeginState::onEnter()
{
	StateParser stateParser;
	stateParser.parseState("assets/attack.xml", s_beginID, &m_gameObjects, &m_textureIDList);

	m_callbacks.push_back(0);
	m_callbacks.push_back(s_beginToMain);
	m_callbacks.push_back(s_beginToPlay);

	setCallbacks(m_callbacks);

	m_loadingComplete = true;

	std::cout << "entering CutsceneBeginState\n";
	return true;
}

bool CutsceneBeginState::onExit()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->clean();
			delete m_gameObjects[i];
		}
		m_gameObjects.clear();
	}
	// clear the texture manager
	for (int i = 0; i < m_textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
	}
	TheInputHandler::Instance()->reset();

	std::cout << "exiting CutsceneBeginState\n";
	return true;
}

void CutsceneBeginState::setCallbacks(const std::vector<Callback>& callbacks)
{
	// go through the game objects
	if (!m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			// if they are of type MenuButton then assign a callback based on the id passed in from the file
			if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
			{
				MenuButton* pButton = dynamic_cast<MenuButton*>(m_gameObjects[i]);
				pButton->setCallback(callbacks[pButton->getCallbackID()]);
			}
		}
	}
}
