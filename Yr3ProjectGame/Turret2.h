//
// Original Code by shaun mitchell. 
//
// Modified by Conor Garrahy & Niall Murphy

#ifndef Turret2__
#define Turret2__
#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class Turret2 : public Enemy
{
public:

	Turret2()
	{
		m_dyingTime = 1000;
		m_health = 8;
		m_bulletFiringSpeed = 75;
	}

	virtual ~Turret2() {}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			m_playerScore += 10; //When the object is destroyed add 10 to players score
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			// we want to scroll this object with the rest
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() - 5, m_position.getY(), 16, 16, "bullet2", 1, Vector2D(0, -10));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 25, m_position.getY(), 16, 16, "bullet2", 1, Vector2D(0, -10));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 40, m_position.getY(), 16, 16, "bullet2", 1, Vector2D(3, -3));
				m_bulletCounter = 0;
			}

			m_bulletCounter++;
		}
		else
		{
			scroll(TheGame::Instance()->getScrollSpeed());
			doDyingAnimation();
		}

	}

};

class Turret2Creator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Turret2();
	}
};


#endif /* defined(__SDL_Game_Programming_Book__Turret__) */
