//
// Original Code by shaun mitchell. 
//
// Modified by Conor Garrahy & Niall Murphy

#ifndef SDL_Game_Programming_Book_Escalator_h
#define SDL_Game_Programming_Book_Escalator_h

#include "Enemy.h"
//updated Eskelator

class Eskeletor : public Enemy
{
public:
    
    virtual ~Eskeletor() {}
    
    Eskeletor() : Enemy()
    {
        m_dyingTime = 50;
        m_health = 1;
        m_moveSpeed = 3;
        m_bulletFiringSpeed = 100;

		m_velocity.setY(-m_moveSpeed);
    }
    
    virtual void collision()
    {
        m_health -= 1;
        
        if(m_health == 0)
        {
            if(!m_bPlayedDeathSound)
            {
                TheSoundManager::Instance()->playSound("explode", 0);
                
                m_textureID = "largeexplosion";
                m_currentFrame = 0;
                m_numFrames = 9;
                m_width = 60;
                m_height = 60;
                m_bDying = true;
            }        
        }
    }
    
    virtual void update()
    {
        if(!m_bDying)
        {
            scroll(TheGame::Instance()->getScrollSpeed());

			if (m_position.getY() + m_height >= TheGame::Instance()->getGameHeight())
			{
				m_velocity.setY(-m_moveSpeed);
			}
			else if (m_position.getY() <= 0)
			{
				m_velocity.setY(m_moveSpeed);
			}
            
            if(m_bulletCounter == m_bulletFiringSpeed)
            {
                TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-3, 0));
                m_bulletCounter = 0;
            }
            m_bulletCounter++;
            
        }
        else
        {
            m_velocity.setY(0);
            doDyingAnimation();
        }
        
        ShooterObject::update();
    }
};

class EskeletorCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new Eskeletor();
    }
};


#endif
