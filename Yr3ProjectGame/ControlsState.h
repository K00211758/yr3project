//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef SDL_Game_Programming_Book_ControlsState_h
#define SDL_Game_Programming_Book_ControlsState_h

#include <iostream>
#include "MenuState.h"
#include <vector>

class GameObject;

class ControlsState : public MenuState
{
public:
       
	virtual ~ControlsState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_controlsID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_controlsToMain();
	static void s_resumePlay();

	static const std::string s_controlsID;

	std::vector<GameObject*> m_gameObjects;
};

#endif