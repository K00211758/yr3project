//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef Charger__
#define Charger__
#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class Charger : public Enemy
{
public:

	virtual ~Charger() {}

	Charger() : Enemy()
	{
		m_bulletFiringSpeed = 70;
		m_dyingTime = 25;
		m_health = 5;
		m_moveSpeed = 1;

		m_velocity.setX(-m_moveSpeed);
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			m_playerScore += 10;//When the object is destroyed add 10 to players score
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams)
	{
		ShooterObject::load(std::move(pParams));

		m_velocity.setX(-m_moveSpeed);
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-10, 0));
				m_bulletCounter = 0;
			}

			m_bulletCounter++;
		}



		else
		{
			m_velocity.setX(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}
};

class ChargerCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Charger();
	}
};


#endif
