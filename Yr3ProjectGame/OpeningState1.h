//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef SDL_Game_Programming_Book_OpeningState1_h
#define SDL_Game_Programming_Book_OpeningState1_h

#include <iostream>
#include "MenuState.h"
#include <vector>

class GameObject;

class OpeningState1 : public MenuState
{
public:

	virtual ~OpeningState1() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_open1ID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_openNext();


	static const std::string s_open1ID;

	std::vector<GameObject*> m_gameObjects;
};

#endif


