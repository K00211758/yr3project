//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef Berserker__
#define Berserker__
#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class Berserker : public Enemy
{
public:

	virtual ~Berserker() {}

	Berserker() : Enemy()
	{
		m_dyingTime = 25;
		m_health = 25;
		m_moveSpeed = 2.5;

		m_velocity.setX(-m_moveSpeed);
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			m_playerScore+=10;//When the object is destroyed add 10 to players score
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams)
	{
		ShooterObject::load(std::move(pParams));

		m_velocity.setX(-m_moveSpeed);
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_position.getX() + m_width >= TheGame::Instance()->getGameWidth())
			{
				m_velocity.setY(-m_moveSpeed);
			}
			else if (m_position.getY() <= 0)
			{
				m_velocity.setY(m_moveSpeed);
			}

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-10, 0));
				m_bulletCounter = 0;
			}

			m_bulletCounter++;
		}
		else
		{
			m_velocity.setX(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}
};

class BerserkerCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Berserker();
	}
};


#endif
