//
//  Coin.h
//  SDL Game Programming Book
//
//  Created by Conor Garrahy and Niall Murphy 1/2/18
//

#ifndef __SDL_Game_Programming_Book__Coin__
#define __SDL_Game_Programming_Book__Coin__

#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class Coin : public Enemy
{
public:

	Coin()
	{
		m_dyingTime = 1000;
		m_health = 5;
		m_bulletFiringSpeed = 50;

		m_textureID = "coins";

		m_currentFrame = 0;
		m_numFrames = 7;
		m_width = 45;
		m_height = 45;
	}

	virtual ~Coin() {}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "flames";

				m_currentFrame = 0;
				m_numFrames = 6;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void update()
	{
		m_currentFrame = int(((SDL_GetTicks() / 100) % 7));

		if (!m_bDying)
		{
			// we want to scroll this object with the rest
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet2", 1, Vector2D(-3, -3));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet2", 1, Vector2D(0, 10));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 40, m_position.getY(), 16, 16, "bullet2", 1, Vector2D(3, -3));
				//m_bulletCounter = 0;
			}

			m_bulletCounter++;
		}
		else
		{
			scroll(TheGame::Instance()->getScrollSpeed());
			doDyingAnimation();
		}

	}

};

class CoinCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Coin();
	}
};


#endif /* defined(__SDL_Game_Programming_Book__Flame__) */

