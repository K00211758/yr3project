//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef SDL_Game_Programming_Book_OpeningState2_h
#define SDL_Game_Programming_Book_OpeningState2_h

#include <iostream>
#include "MenuState.h"
#include <vector>

class GameObject;

class OpeningState2 : public MenuState
{
public:

	virtual ~OpeningState2() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_open2ID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_openNext1();


	static const std::string s_open2ID;

	std::vector<GameObject*> m_gameObjects;
};

#endif




