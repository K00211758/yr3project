//
//  Asteroid.h
//  SDL Game Programming Book
//
//  Created by Conor Garrahy and Niall Murphy 1/2/18
//

#ifndef SDL_Game_Programming_Book_Asteroid_h
#define SDL_Game_Programming_Book_Asteroid_h

#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class Asteroid : public Glider
{
public:

	virtual ~Asteroid() {}

	Asteroid() : Glider()
	{
		
		m_health = 3;
		m_moveSpeed = 5;

		m_velocity.setY(-m_moveSpeed);
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			m_playerScore += 10;//When the object is destroyed add 10 to players score
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_position.getX() + m_width >= TheGame::Instance()->getGameWidth())
			{
				m_velocity.setX(-m_moveSpeed);
			}
			else if (m_position.getX() <= 0)
			{
				m_velocity.setX(m_moveSpeed);
			}

		/*	if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-3, 0));
				m_bulletCounter = 0;
			}
			m_bulletCounter++; */

		}
		else
		{
			m_velocity.setY(0);
			doDyingAnimation();
		}

		 ShooterObject::update();
	}
};

class AsteroidCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Asteroid();
	}
};


#endif

