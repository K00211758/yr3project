//
//  CollisionManager.h
//  SDL Game Programming Book
// Created by Conor Garrahy and Niall Murphy 1/2/18
//  Edited by Conor Garrahy and Niall Murphy 1/2/18
//  Created by shaun mitchell on 28/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef __SDL_Game_Programming_Book__CollisionManager__
#define __SDL_Game_Programming_Book__CollisionManager__

#include <iostream>
#include <vector>

class Player;
class GameObject;
class TileLayer;

class CollisionManager
{
public:
    
    void checkPlayerEnemyBulletCollision(Player* pPlayer);
	void checkPlayerPowerupCollision(Player* pPlayer, const std::vector<GameObject*> &objects);
	void checkPlayerKeyCollision(Player* pPlayer, const std::vector<GameObject*> &objects);
	void checkPlayerBoneCollision(Player* pPlayer, const std::vector<GameObject*> &objects);
    void checkPlayerEnemyCollision(Player* pPlayer, const std::vector<GameObject*> &objects);
    void checkEnemyPlayerBulletCollision(const std::vector<GameObject*>& objects);
    void checkPlayerTileCollision(Player* pPlayer, const std::vector<TileLayer*>& collisionLayers);
};

#endif /* defined(__SDL_Game_Programming_Book__CollisionManager__) */
