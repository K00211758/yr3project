//
// Original Code by shaun mitchell. 
//
// Modified by Conor Garrahy & Niall Murphy

#ifndef SDL_Game_Programming_Book_ShotGlider_h
#define SDL_Game_Programming_Book_ShotGlider_h

#include "Glider.h"

class ShotGlider : public Glider
{
public:
    
    virtual ~ShotGlider() {}
    
    ShotGlider() : Glider()
    {
        m_bulletFiringSpeed = 35;
		m_health = 3;
        m_moveSpeed = 5;

		m_velocity.setY(-m_moveSpeed);
    }

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			m_playerScore += 10;//When the object is destroyed add 10 to players score
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}
    
	virtual void update()
	{
		if (!m_bDying)
		{
			scroll(TheGame::Instance()->getScrollSpeed());

			if (m_position.getX() + m_width >= TheGame::Instance()->getGameWidth())
			{
				m_velocity.setX(-m_moveSpeed);
			}
			else if (m_position.getX() <= 0)
			{
				m_velocity.setX(m_moveSpeed);
			}

			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(-3, 0));
				m_bulletCounter = 0;
			}
			m_bulletCounter++;

		}
		else
		{
			m_velocity.setY(0);
			doDyingAnimation();
		}

		ShooterObject::update();
	}
};

class ShotGliderCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new ShotGlider();
    }
};


#endif
