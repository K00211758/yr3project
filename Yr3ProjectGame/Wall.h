//
//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef __SDL_Game_Programming_Book__Wall__
#define __SDL_Game_Programming_Book__Wall__

#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"

class Wall : public Enemy
{
public:

	Wall()
	{
		m_dyingTime = 1000;
		m_health = 5;
	}

	virtual ~Wall() {}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			m_playerScore += 10;//When the object is destroyed add 10 to players score
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion2";
				m_currentFrame = 0;
				m_numFrames = 1;
				m_width = 60;
				m_height = 60; 
				m_bDying = true; 
			}

		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{
			// we want to scroll this object with the rest
			scroll(TheGame::Instance()->getScrollSpeed());

		/*	if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() - 5, m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(0, 10));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 25, m_position.getY() + 15, 16, 16, "bullet2", 1, Vector2D(0, 10));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 40, m_position.getY(), 16, 16, "bullet2", 1, Vector2D(3, -3));
				m_bulletCounter = 0;
			}

			m_bulletCounter++; */
		}
		else
		{
			scroll(TheGame::Instance()->getScrollSpeed());
			doDyingAnimation();
		}

	}

};

class WallCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Wall();
	}
};


#endif /* defined(__SDL_Game_Programming_Book__Turret__) */

