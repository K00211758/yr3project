//
//  Game.cpp
//  SDL Game Programming Book
// Edited by Conor Garrahy and Niall Murphy 1/2/18
//
#include "Game.h"
#include "TextureManager.h"
#include "InputHandler.h"
#include "MainMenuState.h"
#include "GameObjectFactory.h"
#include "MenuButton.h"
#include "AnimatedGraphic.h"
#include "Player.h"
#include "ScrollingBackground.h"
#include "Flame.h"
#include "SoundManager.h"
#include "RoofTurret.h"
#include "ShotGlider.h"
#include "Powerup.h"
#include "Key.h"
#include "Bone.h"
#include "Coin.h"
#include "Eskeletor.h"
#include "Asteroid.h"
#include "Fireball.h"
#include "Level1Boss.h"
#include "Level2Boss.h"
#include "Level3Boss.h"
#include "GameOverState.h"
#include "LoadingState.h"
#include "OpeningState1.h"
#include "OpeningState2.h"
#include "OpeningState3.h"
#include "Berserker.h"
#include "Invader.h"
#include "Invader2.h"
#include "Wall.h"
#include "Alien.h"
#include "Turret2.h"
#include "Charger.h"
#include <iostream>

using namespace std;

Game* Game::s_pInstance = 0;


Game::Game():
	m_pWindow(0),
	m_pRenderer(0),
	m_bRunning(false),
	m_pGameStateMachine(0),
	m_playerLives(3),
	m_playerPower(3),
	m_playerCoinTotal(0),
	m_scrollSpeed(0.8),
	m_bLevelComplete(false),
	m_bChangingState(false)
{
	// add some level files to an array
	m_levelFiles.push_back("assets/map1.tmx");
	m_levelFiles.push_back("assets/map2.tmx");
	m_levelFiles.push_back("assets/map3.tmx");
	m_levelFiles.push_back("assets/map4.tmx");

	// start at this level
	m_currentLevel = 3;
}

Game::~Game()
{
	// we must clean up after ourselves to prevent memory leaks
	m_pRenderer= 0;
	m_pWindow = 0;
}


bool Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen)
{
	int flags = 0;

	// store the game width and height
	m_gameWidth = width;
	m_gameHeight = height;

	if(fullscreen)
	{
		flags = SDL_WINDOW_FULLSCREEN;
	}

	// attempt to initialise SDL
	if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		cout << "SDL init success\n";
		// init the window
		m_pWindow = SDL_CreateWindow(title, xpos, ypos, width, height, flags);

		if(m_pWindow != 0) // window init success
		{
			cout << "window creation success\n";
			m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED);

			if(m_pRenderer != 0) // renderer init success
			{
				cout << "renderer creation success\n";
				SDL_SetRenderDrawColor(m_pRenderer, 0,0,0,255);
			}
			else
			{
				cout << "renderer init fail\n";
				return false; // renderer init fail
			}
		}
		else
		{
			cout << "window init fail\n";
			return false; // window init fail
		}
	}
	else
	{
		cout << "SDL init fail\n";
		return false; // SDL init fail
	}

	// add some sound effects - TODO move to better place
	TheSoundManager::Instance()->load("assets/DST_ElectroRock2.ogg", "music1", SOUND_MUSIC);
	TheSoundManager::Instance()->load("assets/boom.wav", "explode", SOUND_SFX);
	TheSoundManager::Instance()->load("assets/phaser2.wav", "shoot", SOUND_SFX);

	TheSoundManager::Instance()->playMusic("music1", -1);

	//TheInputHandler::Instance()->initialiseJoysticks();

	// register the types for the game
	TheGameObjectFactory::Instance()->registerType("MenuButton", new MenuButtonCreator());
	TheGameObjectFactory::Instance()->registerType("Player", new PlayerCreator());
	TheGameObjectFactory::Instance()->registerType("AnimatedGraphic", new AnimatedGraphicCreator());
	TheGameObjectFactory::Instance()->registerType("ScrollingBackground", new ScrollingBackgroundCreator());
	TheGameObjectFactory::Instance()->registerType("Turret", new TurretCreator());
	TheGameObjectFactory::Instance()->registerType("Turret2", new Turret2Creator());
	TheGameObjectFactory::Instance()->registerType("Glider", new GliderCreator());
	TheGameObjectFactory::Instance()->registerType("ShotGlider", new ShotGliderCreator());
	TheGameObjectFactory::Instance()->registerType("Berserker", new BerserkerCreator());
	TheGameObjectFactory::Instance()->registerType("Powerup", new PowerupCreator());
	TheGameObjectFactory::Instance()->registerType("Key", new KeyCreator());
	TheGameObjectFactory::Instance()->registerType("Bone", new BoneCreator());
	TheGameObjectFactory::Instance()->registerType("RoofTurret", new RoofTurretCreator());
	TheGameObjectFactory::Instance()->registerType("Eskeletor", new EskeletorCreator());
	TheGameObjectFactory::Instance()->registerType("Charger", new ChargerCreator());
	TheGameObjectFactory::Instance()->registerType("Asteroid", new AsteroidCreator());
	TheGameObjectFactory::Instance()->registerType("Invader", new InvaderCreator());
	TheGameObjectFactory::Instance()->registerType("Invader2", new Invader2Creator());
	TheGameObjectFactory::Instance()->registerType("Fireball", new FireballCreator());
	TheGameObjectFactory::Instance()->registerType("Level1Boss", new Level1BossCreator());
	TheGameObjectFactory::Instance()->registerType("Level2Boss", new Level2BossCreator());
	TheGameObjectFactory::Instance()->registerType("Level3Boss", new Level3BossCreator());
	TheGameObjectFactory::Instance()->registerType("Wall", new WallCreator());
	TheGameObjectFactory::Instance()->registerType("Alien", new AlienCreator());
	TheGameObjectFactory::Instance()->registerType("Flame", new FlameCreator());
	TheGameObjectFactory::Instance()->registerType("Coin", new CoinCreator());

	// start the menu state
	m_pGameStateMachine = new GameStateMachine();
	m_pGameStateMachine->changeState(new MainMenuState());

	m_bRunning = true; // everything inited successfully, start the main loop
	return true;
}

void Game::setCurrentLevel(int currentLevel)
{
	m_currentLevel = currentLevel;
	m_pGameStateMachine->changeState(new LoadingState());
	m_bLevelComplete = false;
}

/*void Game::setNextLevel(int nextLevel)
{
	m_nextLevel = nextLevel;
	nextLevel = m_currentLevel + 1;
	m_nextLevel = nextLevel;
} */

void Game::render()
{
	SDL_RenderClear(m_pRenderer);


	m_pGameStateMachine->render();


	SDL_RenderPresent(m_pRenderer);
}

void Game::update()
{

	m_pGameStateMachine->update();

}

void Game::handleEvents()
{

	TheInputHandler::Instance()->update();

}

void Game::clean()
{
	cout << "cleaning game\n";

	TheInputHandler::Instance()->clean();

	m_pGameStateMachine->clean();

	m_pGameStateMachine = 0;
	delete m_pGameStateMachine;

	TheTextureManager::Instance()->clearTextureMap();

	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);
	SDL_Quit();
}


