//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef SDL_Game_Programming_Book_OpeningState3_h
#define SDL_Game_Programming_Book_OpeningState3_h

#include <iostream>
#include "MenuState.h"
#include <vector>

class GameObject;

class OpeningState3 : public MenuState
{
public:

	virtual ~OpeningState3() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_open3ID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_openBegin();


	static const std::string s_open3ID;

	std::vector<GameObject*> m_gameObjects;
};

#endif






