//
//  CutsceneBeginState.h
//  SDL Game Programming Book
//
//  Created by Conor Garrahy and Niall Murphy 1/2/18
//

#ifndef __SDL_Game_Programming_Book__CutsceneBeginState__
#define __SDL_Game_Programming_Book__CutsceneBeginState__

#include <iostream>
#include <vector>
#include "MenuState.h"
#include "PlayState.h"

class GameObject;

class CutsceneBeginState : public MenuState
{
public:

	virtual ~CutsceneBeginState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_beginID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_beginToMain();
	static void s_beginToPlay();

	static const std::string s_beginID;

	std::vector<GameObject*> m_gameObjects;
};


#endif /* defined(__SDL_Game_Programming_Book__CutsceneBeginState__) */

