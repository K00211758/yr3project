//
//  Key.h
//  SDL Game Programming Book
//
//  Created by Conor Garrahy and Niall Murphy 1/2/18
//

#ifndef SDL_Game_Programming_Book_Key_h
#define SDL_Game_Programming_Book_Key_h

#include <iostream>
#include "GameObjectFactory.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include <math.h>
#include "Enemy.h"
#include "Game.h"



class Key : public Enemy
{
public:

	

	Key() : Enemy()
	{
		m_health = 1;
		m_dyingTime = 25;

		m_moveSpeed = 2.5;
		m_gap = 60;

	}

	virtual ~Key() {}

	virtual void load(std::unique_ptr<LoaderParams> const &pParams)
	{
		ShooterObject::load(std::move(pParams));

		m_velocity.setY(m_moveSpeed / 2);

		m_maxHeight = m_position.getY() + m_gap;
		m_minHeight = m_position.getY() - m_gap;
	}

	virtual void collision()
	{
			m_health -= 1;

			if (m_health == 0)
			{
				if (!m_bPlayedDeathSound)
				{

					TheSoundManager::Instance()->playSound("explode", 0);

					m_textureID = "reward";
					m_currentFrame = 0;
					m_numFrames = 1;
					m_width = 38;
					m_height = 34;
					m_bDying = true;

					TheGame::Instance()->setLevelComplete(true);
				}

			}
		
	}

	virtual void update()
	{
			if (!m_bDying)
			{
				scroll(TheGame::Instance()->getScrollSpeed());

				if (m_position.getY() >= m_maxHeight)
				{
					m_velocity.setY(-m_moveSpeed);
				}
				else if (m_position.getY() <= m_minHeight)
				{
					m_velocity.setY(m_moveSpeed);
				}
			}
			else
			{
				m_velocity.setY(0);
				doDyingAnimation();
			}

			ShooterObject::update();
		

	}

	virtual std::string type()
	{
		return "Key";
	}

private:

	int m_maxHeight;
	int m_minHeight;
	int m_gap;
};

class KeyCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Key();
	}
};


#endif

