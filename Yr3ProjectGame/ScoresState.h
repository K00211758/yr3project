//  Created by Conor Garrahy and Niall Murphy 1/2/18

#ifndef SDL_Game_Programming_Book_ScoresState_h
#define SDL_Game_Programming_Book_ScoresState_h

#include <iostream>
#include "MenuState.h"
#include <vector>

class GameObject;

class ScoresState : public MenuState
{
public:

	virtual ~ScoresState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_scoresID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_scoresToMain();

	static const std::string s_scoresID;

	std::vector<GameObject*> m_gameObjects;
};

#endif

